package net.mvcproject.form;

import java.util.List;
import java.util.Map;

public class ProfesionalForm {

	private List<Map<String, String>> profesionales;

	public List<Map<String, String>> getProfesionales() {
		return profesionales;
	}

	public void setProfesionales(List<Map<String, String>> profesionales) {
		this.profesionales = profesionales;
	}

}
