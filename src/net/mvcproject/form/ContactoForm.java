package net.mvcproject.form;

public class ContactoForm {

	private String motivo;
	private String nombre;
	private String apellido;
	private String email;
	private String telefono;
	private String mensaje;
	private String generalErrors;

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getGeneralErrors() {
		return generalErrors;
	}

	public void setGeneralErrors(String generalErrors) {
		this.generalErrors = generalErrors;
	}

}
