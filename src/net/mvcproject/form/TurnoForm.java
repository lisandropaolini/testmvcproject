package net.mvcproject.form;

import java.util.List;
import java.util.Map;

public class TurnoForm {

	private String obraSocial;
	private List<String> obraSocialList;
	private String plan;
	private List<String> plansList;
	private String tipo;
	private String especialidad;
	private List<String> especialidadList;
	private String profesional;
	private List<Map<String, String>> profesionalList;
	private String fecha;
	private List<String> fechaList;
	private String hora;
	private List<String> horaList;
	private String nombre;
	private String email;
	private String mark;

	public String getObraSocial() {
		return obraSocial;
	}

	public void setObraSocial(String obraSocial) {
		this.obraSocial = obraSocial;
	}

	public List<String> getObraSocialList() {
		return obraSocialList;
	}

	public void setObraSocialList(List<String> obraSocialList) {
		this.obraSocialList = obraSocialList;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public List<String> getPlansList() {
		return plansList;
	}

	public void setPlansList(List<String> plansList) {
		this.plansList = plansList;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public List<String> getEspecialidadList() {
		return especialidadList;
	}

	public void setEspecialidadList(List<String> especialidadList) {
		this.especialidadList = especialidadList;
	}

	public String getProfesional() {
		return profesional;
	}

	public void setProfesional(String profesional) {
		this.profesional = profesional;
	}

	public List<Map<String, String>> getProfesionalList() {
		return profesionalList;
	}

	public void setProfesionalList(List<Map<String, String>> profesionalList) {
		this.profesionalList = profesionalList;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	public String getHora() {
		return hora;
	}
	
	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<String> getFechaList() {
		return fechaList;
	}

	public void setFechaList(List<String> fechaList) {
		this.fechaList = fechaList;
	}
	
	public List<String> getHoraList() {
		return horaList;
	}
	
	public void setHoraList(List<String> horaList) {
		this.horaList = horaList;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}

}
