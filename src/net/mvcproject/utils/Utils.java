package net.mvcproject.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class Utils {

	public static String getParseDay(int dayPlus){
		SimpleDateFormat formattedDate = new SimpleDateFormat("dd/MM/yyyy");            
		Calendar c = Calendar.getInstance();        
		c.add(Calendar.DATE, dayPlus);      
		return (String)(formattedDate.format(c.getTime()));
	}
	
	public static String getParseHours(int hoursPlus){
		if (hoursPlus < 10)
			return "0"+hoursPlus+":00";
		else
			return hoursPlus+":00";
	}
}
