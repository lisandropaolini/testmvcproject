package net.mvcproject.service;

import java.util.List;
import java.util.Map;

public interface ProfesionalService {
	public List<Map<String, String>> getProfesionales() throws Exception;
	public List<String> getEspecialidades() throws Exception;
	public List<Map<String, String>> getProfesionalesByEspecialidad(String especialidad) throws Exception;
}
