package net.mvcproject.service;

import java.util.Map;

public interface ContactoService {
	public void addContacto(Map<String, String> userData) throws Exception;
}
