package net.mvcproject.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.mvcproject.dataaccess.TurnoDao;
import net.mvcproject.entities.Turno;
import net.mvcproject.service.TurnoService;

@Service("turnoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class TurnoServiceImpl implements TurnoService {

	@Autowired
	private TurnoDao turnoDao;
	
	@Override
	public void addTurno(Map<String, String> userData) {
		Turno contacto = new Turno();
		
		contacto.setPrestacion(userData.get("prestacion"));
		contacto.setPlan(userData.get("plan"));
		contacto.setTipo(userData.get("tipo"));
		contacto.setEspecialidad(userData.get("especialidad"));
		contacto.setProfesional(userData.get("profesional"));
		contacto.setTime(userData.get("time"));
		contacto.setEmail(userData.get("email"));
		contacto.setPaciente(userData.get("paciente"));
		contacto.setDate(userData.get("date"));

		turnoDao.addTurno(contacto);
		
	}

}
