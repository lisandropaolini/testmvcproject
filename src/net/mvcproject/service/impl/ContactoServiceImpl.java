package net.mvcproject.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.mvcproject.dataaccess.ContactoDao;
import net.mvcproject.entities.Contacto;
import net.mvcproject.service.ContactoService;

@Service("contactoService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ContactoServiceImpl implements ContactoService {

	@Autowired
	private ContactoDao contactoDao;

	public ContactoServiceImpl() {
	}

	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public void addContacto(Map<String, String> userData) throws Exception {

		Contacto contacto = new Contacto();

		contacto.setMotivo(userData.get("motivo"));
		contacto.setNombre(userData.get("nombre"));
		contacto.setApellido(userData.get("apellido"));
		contacto.setEmail(userData.get("email"));
		contacto.setTelefono(userData.get("telefono"));
		contacto.setMensaje(userData.get("mensaje"));

		contactoDao.addContacto(contacto);

	}

}
