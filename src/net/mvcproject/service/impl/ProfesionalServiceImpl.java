package net.mvcproject.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import net.mvcproject.dataaccess.ProfesionalDao;
import net.mvcproject.entities.Profesional;
import net.mvcproject.service.ProfesionalService;

@Service("profesionalService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ProfesionalServiceImpl implements ProfesionalService {

	@Autowired
	private ProfesionalDao profesionalDao;

	public ProfesionalServiceImpl() {
	}

	@Override
	public List<Map<String, String>> getProfesionales() throws Exception {
		List<Profesional> profesionalesList = profesionalDao.getProfesionales();
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		if (profesionalesList != null && !profesionalesList.isEmpty()) {
			for (Profesional pro : profesionalesList) {
				Map<String, String> elemento = new HashMap<String, String>();
				elemento.put("name", pro.getName());
				elemento.put("cv", pro.getCv());
				elemento.put("surname", pro.getSurname());
				elemento.put("especialidad", pro.getEspecialidad());
				result.add(elemento);
			}
		}
		return result;
	}
	
	@Override
	public List<Map<String, String>> getProfesionalesByEspecialidad(String especialidad) throws Exception {
		List<Profesional> profesionalesList = profesionalDao.getProfesionalesByEspecialidad(especialidad);
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		if (profesionalesList != null && !profesionalesList.isEmpty()) {
			for (Profesional pro : profesionalesList) {
				Map<String, String> elemento = new HashMap<String, String>();
				elemento.put("name", pro.getName());
				elemento.put("cv", pro.getCv());
				elemento.put("surname", pro.getSurname());
				elemento.put("especialidad", pro.getEspecialidad());
				result.add(elemento);
			}
		}
		return result;
	}

	@Override
	public List<String> getEspecialidades() throws Exception {
		return profesionalDao.getEspecialidades();
	}

}
