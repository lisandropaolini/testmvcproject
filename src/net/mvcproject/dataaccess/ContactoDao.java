package net.mvcproject.dataaccess;

import net.mvcproject.entities.Contacto;

public interface ContactoDao {
	public void addContacto(Contacto contacto);
}
