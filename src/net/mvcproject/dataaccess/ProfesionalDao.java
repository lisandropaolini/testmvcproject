package net.mvcproject.dataaccess;

import java.util.List;

import net.mvcproject.entities.Profesional;

public interface ProfesionalDao {

	public List<Profesional> getProfesionales();
	public List<String> getEspecialidades();
	public List<Profesional> getProfesionalesByEspecialidad(String especialidad);
}
