package net.mvcproject.dataaccess.impl;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.mvcproject.dataaccess.TurnoDao;
import net.mvcproject.entities.Turno;

@Repository("turnoDao")
public class TurnoDaoImpl implements TurnoDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addTurno(Turno turno) {
		turno.setAddedDate(new Date());
		sessionFactory.getCurrentSession().saveOrUpdate(turno);
	}
}
