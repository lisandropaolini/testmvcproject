package net.mvcproject.dataaccess.impl;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.mvcproject.dataaccess.ContactoDao;
import net.mvcproject.entities.Contacto;

@Repository("contactoDao")
public class ContactoDaoImpl implements ContactoDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addContacto(Contacto contacto) {
		contacto.setAddedDate(new Date());
		sessionFactory.getCurrentSession().saveOrUpdate(contacto);
	}

}
