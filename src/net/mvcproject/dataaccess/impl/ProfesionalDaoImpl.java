package net.mvcproject.dataaccess.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import net.mvcproject.dataaccess.ProfesionalDao;
import net.mvcproject.entities.Profesional;

@Repository("profesionalDao")
public class ProfesionalDaoImpl implements ProfesionalDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Profesional> getProfesionales() {

		List<Profesional> salida = sessionFactory.getCurrentSession().createCriteria(Profesional.class).list();

		return salida;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Profesional> getProfesionalesByEspecialidad(String especialidad) {

		List<Profesional> salida = sessionFactory.getCurrentSession().createCriteria(Profesional.class)
				.add(Restrictions.like("especialidad", especialidad)).list();

		return salida;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> getEspecialidades() {
		List<String> especialidades = sessionFactory.getCurrentSession().createCriteria(Profesional.class)
				.setProjection(Projections.alias(Projections.groupProperty("especialidad"), "especialidad"))
				.addOrder(Order.desc("especialidad")).list();

		return especialidades;
	}

}
