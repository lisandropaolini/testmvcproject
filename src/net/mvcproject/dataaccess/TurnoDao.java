package net.mvcproject.dataaccess;

import net.mvcproject.entities.Turno;

public interface TurnoDao {

	public void addTurno(Turno turno);
}
