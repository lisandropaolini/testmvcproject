package net.mvcproject.controllers;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.mvcproject.form.ProfesionalForm;
import net.mvcproject.service.ProfesionalService;

@Controller
@RequestMapping("/profesionales.html")
public class ProfesionalController {

	@Autowired
	ProfesionalService profesionalService;

	@RequestMapping(method = RequestMethod.GET)
	public String showProfesionales(Map model) {
		ProfesionalForm profesionalForm = new ProfesionalForm();

		try {
			List<Map<String, String>> proList = profesionalService.getProfesionales();

			profesionalForm.setProfesionales(proList);
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.put("profesionalForm", profesionalForm);
		return "profesionales";
	}

}
