package net.mvcproject.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.mvcproject.form.TurnoForm;
import net.mvcproject.service.ProfesionalService;
import net.mvcproject.service.TurnoService;
import net.mvcproject.utils.Utils;

@Controller
@RequestMapping("/turno.html")
public class TurnoController {

	@Autowired
	ProfesionalService profesionalService;

	@Autowired
	TurnoService turnoService;

	@RequestMapping(method = RequestMethod.GET)
	public String showForm1(TurnoForm turnoForm, HttpSession session) {

		turnoForm = cargaForm(turnoForm, session);

		return "turno";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String showForm(TurnoForm turnoForm, HttpSession session) {

		if ("submit".equals(turnoForm.getMark())) {
			Map<String, String> userData = new HashMap<String, String>();
			userData.put("prestacion", turnoForm.getObraSocial());
			userData.put("plan", turnoForm.getPlan());
			userData.put("tipo", turnoForm.getTipo());
			userData.put("especialidad", turnoForm.getEspecialidad());
			userData.put("profesional", turnoForm.getProfesional());
			userData.put("date", turnoForm.getFecha());
			userData.put("time", turnoForm.getHora());
			userData.put("paciente", turnoForm.getNombre());
			userData.put("email", turnoForm.getEmail());

			try {
				turnoService.addTurno(userData);
				return "turnosuccess";
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		turnoForm = cargaForm(turnoForm, session);
		return "turno";
	}

	private TurnoForm cargaForm(TurnoForm turnoForm, HttpSession session) {
		try {
			List<String> especialidades = profesionalService.getEspecialidades();
			turnoForm.setEspecialidadList(especialidades);

			if (especialidades != null && !especialidades.isEmpty()) {
				if (turnoForm.getEspecialidad() == null || turnoForm.getEspecialidad().isEmpty())
					turnoForm.setEspecialidad(especialidades.get(0));

				turnoForm.setProfesionalList(
						profesionalService.getProfesionalesByEspecialidad(turnoForm.getEspecialidad()));
			}

			List<String> fechas = new ArrayList<String>();
			for (int i = 0; i < 6; i++) {
				fechas.add(Utils.getParseDay(i));
			}
			turnoForm.setFechaList(fechas);

			List<String> horas = new ArrayList<String>();
			for (int i = 9; i < 18; i++) {
				horas.add(Utils.getParseHours(i));
			}
			turnoForm.setHoraList(horas);

			@SuppressWarnings("unchecked")
			Map<String, String> userData = (Map<String, String>) session.getAttribute("user");
			turnoForm.setNombre(userData.get("name"));
			turnoForm.setEmail(userData.get("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return turnoForm;
	}

}
