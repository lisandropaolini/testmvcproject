package net.mvcproject.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import net.mvcproject.form.ContactoForm;
import net.mvcproject.service.ContactoService;

@Controller
@RequestMapping("/contactoform.html")
public class ContactoController {

	@Autowired
	private ContactoValidation contactoValidation;

	@Autowired
	ContactoService contactoService;

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Map model) {
		ContactoForm contactoForm = new ContactoForm();
		model.put("contactoForm", contactoForm);
		return "contactoform";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(@Valid ContactoForm contactoForm, BindingResult result) {

		contactoValidation.validate(contactoForm, result);
		if (result.hasErrors()) {
			return "contactoform";
		}

		Map<String, String> userData = new HashMap<String, String>();
		userData.put("apellido", contactoForm.getApellido());
		userData.put("motivo", contactoForm.getMotivo());
		userData.put("nombre", contactoForm.getNombre());
		userData.put("email", contactoForm.getEmail());
		userData.put("telefono", contactoForm.getTelefono());
		userData.put("mensaje", contactoForm.getMensaje());

		try {
			contactoService.addContacto(userData);

		} catch (Exception e) {
			e.printStackTrace();
			result.rejectValue("generalErrors", "user.registration.general.error", e.getMessage());

			return "contactoform";
		}

		return "contactosuccess";

	}
}
