package net.mvcproject.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/home.html")
public class HomeController {

	@RequestMapping(method = RequestMethod.GET)
	public String quienessomos() {

		return "home";
	}

}
