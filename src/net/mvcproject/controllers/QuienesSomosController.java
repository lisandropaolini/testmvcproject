package net.mvcproject.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/quienessomos.html")
public class QuienesSomosController {

	@RequestMapping(method = RequestMethod.GET)
	public String quienessomos() {

		return "quienessomos";
	}
}
