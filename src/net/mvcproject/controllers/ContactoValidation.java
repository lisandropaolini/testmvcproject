package net.mvcproject.controllers;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import net.mvcproject.form.ContactoForm;

@Component("contactoValidation")
public class ContactoValidation {

	public boolean supports(Class<?> klass) {
		return ContactoForm.class.isAssignableFrom(klass);
	}
	
	public void validate(Object target, Errors errors) {
		
	}
}
