package net.mvcproject.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "turnos")
public class Turno {

	@Id
	@GeneratedValue
	@Column(name = "turno_id")
	private Long turnoId;

	@Column(name = "prestacion", nullable = false, length = 50)
	private String prestacion;
	@Column(name = "plan", nullable = true, length = 50)
	private String plan;
	@Column(name = "tipo", nullable = false, length = 50)
	private String tipo;
	@Column(name = "especialidad", nullable = false, length = 50)
	private String especialidad;
	@Column(name = "profesional", nullable = false, length = 50)
	private String profesional;
	@Column(name = "time", nullable = false, length = 5)
	private String time;
	@Column(name = "date", nullable = false, length = 10)
	private String date;
	@Column(name = "paciente", nullable = false, length = 50)
	private String paciente;
	@Column(name = "email", nullable = false, length = 50)
	private String email;
	@Column(name = "added_date")
	private Date addedDate;

	public Long getTurnoId() {
		return turnoId;
	}

	public void setTurnoId(Long turnoId) {
		this.turnoId = turnoId;
	}

	public String getPrestacion() {
		return prestacion;
	}

	public void setPrestacion(String prestacion) {
		this.prestacion = prestacion;
	}

	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public String getProfesional() {
		return profesional;
	}

	public void setProfesional(String profesional) {
		this.profesional = profesional;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPaciente() {
		return paciente;
	}

	public void setPaciente(String paciente) {
		this.paciente = paciente;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

}
