package net.mvcproject.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "professional")
public class Profesional {

	@Id
	@GeneratedValue
	@Column(name = "pro_id")
	private Long proId;

	@Column(name = "name", nullable = false, length = 50)
	private String name;
	@Column(name = "cv", nullable = false, length = 50)
	private String cv;
	@Column(name = "surname", nullable = false, length = 50)
	private String surname;
	@Column(name = "especialidad", nullable = false, length = 50)
	private String especialidad;

	@Column(name = "added_date")
	private Date addedDate;

	public Long getProId() {
		return proId;
	}

	public void setProId(Long proId) {
		this.proId = proId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCv() {
		return cv;
	}

	public void setCv(String cv) {
		this.cv = cv;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(String especialidad) {
		this.especialidad = especialidad;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}

}
