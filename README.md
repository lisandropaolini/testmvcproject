For this simple project I use MySql 5.5, the database name is "db_mvcproject", the user and password for this database is "mvcproject".
The Web Server I use for this project is Tomcat 6.


This is the sql script for create de Table that I use in this project:
 CREATE TABLE user (
 user_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 username VARCHAR(50) NOT NULL,
 password VARCHAR(50) NOT NULL,
 email VARCHAR(50) NOT NULL,
 name VARCHAR(50) NOT NULL,
 surname VARCHAR(50) NOT NULL,
 added_date DATETIME NOT NULL);
 
 
  CREATE TABLE professional (
 pro_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(50) NOT NULL,
 cv VARCHAR(6553) NOT NULL,
 surname VARCHAR(50) NOT NULL,
 especialidad VARCHAR(100) NOT NULL,
 added_date DATETIME NOT NULL);
 
   CREATE TABLE turnos (
 turno_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 prestacion VARCHAR(50) NOT NULL,
 plan VARCHAR(50),
 tipo VARCHAR(50) NOT NULL,
 especialidad VARCHAR(50) NOT NULL,
 profesional VARCHAR(50) NOT NULL,
 time VARCHAR(5) NOT NULL,
 date VARCHAR(10) NOT NULL,
 paciente VARCHAR(50) NOT NULL,
 email VARCHAR(50) NOT NULL,
 added_date DATETIME NOT NULL);
 
 
CREATE TABLE contacto (
 consulta_id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 motivo VARCHAR(50) NOT NULL,
nombre VARCHAR(50) NOT NULL,
apellido VARCHAR(50) NOT NULL,
email VARCHAR(50) NOT NULL,
telefono VARCHAR(50) NOT NULL,
mensaje VARCHAR(1024) NOT NULL,
 added_date DATETIME NOT NULL);
 
 