<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css"
	rel="stylesheet" type="text/css">
<title>Test MVC Project</title>

<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/JavaScript"
	src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/JavaScript"
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/JavaScript">
	$(document).ready(function() {
		$("#myTableId").dataTable();
		
		var str = document.getElementById("myTableId_wrapper").innerHTML; 
		var res = str.replace("Search", "Buscar");
		var res = res.replace("Show", "Mostrar");
		var res = res.replace("entries", "registros");
		var res = res.replace("Previous", "Anterior");
		var res = res.replace("Next", "Pr�ximo");
		document.getElementById("myTableId_wrapper").innerHTML = res;
		
		var dti = document.getElementById("myTableId_info").innerHTML; 
		var dtis = dti.replace("of", "de");
		var dtis = dtis.replace("Showing", "Mostrando");
		var dtis = dtis.replace("to", "a");
		var dtis = dtis.replace("entries", "registros");
		document.getElementById("myTableId_info").innerHTML = dtis;
		
	});
</script>

</head>
<body>


	<%@include file="header.jsp"%>

	<p style="text-align: center">
	<h1>Nuestros Profesionales</h1>
	</p>



	<table id="myTableId" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<td></td>
				<td><b>Nombre</b></td>
				<td><b>Apellido</b></td>
				<td><b>Especialidad</b></td>
				<td><b>CV</b></td>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${profesionalForm.profesionales}" var="profesional">
				<tr>
					<td>-</td>
					<td><c:out value="${profesional.name}" /></td>
					<td><c:out value="${profesional.surname}" /></td>
					<td><c:out value="${profesional.especialidad}" /></td>
					<td>x</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</body>
</html>