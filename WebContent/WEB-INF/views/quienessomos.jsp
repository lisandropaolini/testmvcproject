<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css"
	rel="stylesheet" type="text/css">
<title>Test MVC Project</title>
</head>
<body>

	<%@include file="header.jsp"%>

	<div class="texto">

		<h3 class="title">Qui&eacute;nes somos?</h3>
		<div class="contenido">
			<blockquote>
				<p>Sanatorio de la ciudad de C&oacute;rdoba</p>
			</blockquote>
			<p>
				Sanatorio de la ciudad de C&oacute;rdoba busca brindar la
				m&aacute;xima calidad en el cuidado a todos sus pacientes, todos los
				d&iacute;as, a trav&eacute;s de la pr&aacute;ctica m&eacute;dica
				integral, a cargo de un equipo de profesionales altamente
				calificados tanto t&eacute;cnica como humanamente, con el soporte de
				una moderna infraestructura y tecnolog&iacute;a de avanzada, a
				trav&eacute;s de la investigaci&oacute;n, capacitaci&oacute;n y
				docencia de todos sus cuadros. <br> El Sanatorio fue fundado en
				1979 por el Dr. Oscar Eduardo Muller con el objetivo de ofrecer en
				C&oacute;rdoba, un servicio m&eacute;dico de alta jerarqu&iacute;a
				profesional dentro de una m&aacute;xima prestaci&oacute;n de
				servicio. A partir de entonces, convencidos y comprometidos con esta
				filosof&iacute;a de trabajo, el Sanatorio fue creciendo paso a paso,
				incorporando nuevas tecnolog&iacute;as, equipamiento,
				infraestructura y modernos servicios acompa&ntilde;ando el avance y
				la especializaci&oacute;n de las ciencias de la salud.
			</p>
			<p style="text-align: center">
				<img alt="" src="../images/int.png" width="300px">
			</p>
			<p>La conducci&oacute;n del Sanatorio est&aacute; a cargo del
				Honorable Directorio y de un Director de quienes dependen las
				&aacute;reas M&eacute;dica, Administrativa y Comercial. Actualmente
				rige un sistema de organizaci&oacute;n m&eacute;dica departamental,
				habi&eacute;ndose dividido en los siguientes departamentos:</p>
			<ul>
				<li>Ambulatorio</li>
				<li>Docencia e investigaci&oacute;n</li>
				<li>Internado</li>
				<li>Quir&oacute;fano</li>
				<li>Servicios Auxiliares</li>
			</ul>
			<p>De cada uno de ellos dependen Servicios y Secciones que
				corresponden a las diversas especialidades.</p>
		</div>
	</div>

</body>
</html>