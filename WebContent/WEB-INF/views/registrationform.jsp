<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css"
	rel="stylesheet" type="text/css">
<title>Test MVC Project</title>

<script type="text/JavaScript"
	src="https://code.jquery.com/jquery-1.12.4.js"></script>


<script type="text/JavaScript">
	$(document).ready(function() {
		var str = document.getElementById("myTableId_form").innerHTML; 
		var res = str.replace("may not be empty", "no debe estar vac�o");
		res = res.replace("may not be empty", "no debe estar vac�o");
		res = res.replace("may not be empty", "no debe estar vac�o");
		res = res.replace("may not be empty", "no debe estar vac�o");
		res = res.replace("may not be empty", "no debe estar vac�o");
		res = res.replace("may not be empty", "no debe estar vac�o");
		document.getElementById("myTableId_form").innerHTML = res;
	});
</script>

</head>
<body>

	<%@include file="header.jsp"%>


	<form:form method="Post" action="registrationform.html"
		commandName="registration">
		<table align="center" id="myTableId_form">
			<tr>
				<td colspan="2" align="center"><h3>Formulario de Registro</h3></td>
			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="generalErrors" /></FONT></td>
			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="userName" /></FONT></td>
			</tr>
			<tr>
				<td>Usuario:</td>
				<td><form:input path="userName" /></td>

			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="password" /></FONT></td>
			</tr>
			<tr>
				<td>Contrase&ntilde;a:</td>
				<td><form:password path="password" /></td>

			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="confirmPassword" /></FONT></td>
			</tr>
			<tr>
				<td>Confirmar Contrase&ntilde;a:</td>
				<td><form:password path="confirmPassword" /></td>

			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="email" /></FONT></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input path="email" /></td>

			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="name" /></FONT></td>
			</tr>
			<tr>
				<td>Nombre:</td>
				<td><form:input path="name" /></td>

			</tr>
			<tr>
				<td colspan="2"><FONT color="red"><form:errors
							path="surname" /></FONT></td>
			</tr>
			<tr>
				<td>Apellido:</td>
				<td><form:input path="surname" /></td>

			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input type="submit"
					value="Registrarse" class="buttonStyle" /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><a href="loginform.html">Volver
						al Login</a></td>
			</tr>
		</table>
	</form:form>
</body>
</html>