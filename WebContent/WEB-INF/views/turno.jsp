<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css"
	rel="stylesheet" type="text/css">
	
<script type="text/javascript">
function myJsFunc(val) {
    document.getElementById("mark").value = val;
    document.getElementById("turnoForm").submit();
}
</script>	
	
<title>Test MVC Project</title>
</head>
<body>

	<%@include file="header.jsp"%>

	<form:form method="Post" action="turno.html" commandName="turnoForm" id="turnoForm" name="turnoForm">
	<form:hidden path="mark" id="mark" value="lisa"/>
		<table align="center">

			<tr>
				<td colspan="2" align="center"><h1>Turnos</h1></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><h2>Datos</h2></td>
			</tr>
			<tr>
				<td width="200px;">Prestaci&oacute;n:</td>
				<td width="100px;"><form:select path="obraSocial"
						style="width: 140px;" onchange="this.form.submit()">
						<form:option value="ObraSocial">Obra Social</form:option>
						<form:option value="Particular">Particular</form:option>
					</form:select></td>
			</tr>

<%
String obraSocial = request.getParameter("obraSocial");
%>
			<c:set var="obraSocial" value="<%=obraSocial%>" />
			<c:if test="${(obraSocial != 'Particular')}">
				<tr>
					<td>Plan:</td>
					<td width="100"><form:select path="plan"
						style="width: 140px;">
						<form:option value="OSDE-123">OSDE 123</form:option>
						<form:option value="PAMI">PAMI</form:option>
						<form:option value="APROSS">APROSS</form:option>
					</form:select></td>
				</tr>
			</c:if>

			<tr>
				<td>Tipo:</td>
				<td><form:select path="tipo" style="width: 140px;">
						<form:option value="Consulta">Consulta</form:option>
						<form:option value="Procedimiento">Procedimiento</form:option>
					</form:select></td>
			</tr>
			<tr>
				<td>Especialidad / Practica:</td>
				<td>
				<form:select path="especialidad" style="width: 140px;" onchange="this.form.submit()">
					<option value=""></option>
				  <c:forEach items="${turnoForm.especialidadList}" var="databaseValue">
				    <form:option value="${databaseValue}">
				        ${databaseValue}
				    </form:option>
				  </c:forEach>
				</form:select>
				
				</td>
			</tr>
			<tr>
				<td>Profesional:</td>
				<td><form:select path="profesional" style="width: 140px;">
				  <c:forEach items="${turnoForm.profesionalList}" var="databaseValue">
				    <form:option value="${databaseValue.name} ${databaseValue.surname}">
				        ${databaseValue.name} ${databaseValue.surname}
				    </form:option>
				  </c:forEach>
				</form:select></td>
			</tr>

			<tr>
				<td>Fecha:</td>
				<td><form:select path="fecha" style="width: 140px;">
				<c:forEach items="${turnoForm.fechaList}" var="databaseValue">
				    <form:option value="${databaseValue}">
				        ${databaseValue}
				    </form:option>
				  </c:forEach>
				</form:select></td>
			</tr>

			<tr>
				<td>Hora:</td>
				<td><form:select path="hora" style="width: 140px;">
				<c:forEach items="${turnoForm.horaList}" var="databaseValue">
				    <form:option value="${databaseValue}">
				        ${databaseValue}
				    </form:option>
				  </c:forEach>
				</form:select></td>
			</tr>

			<tr>
				<td>Nombre y Apellido:</td>
				<td><form:input path="nombre" readonly="true"/></td>
			</tr>
			<tr>
				<td>Email:</td>
				<td><form:input path="email" readonly="true"/></td>
			</tr>

			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="2" align="center"><a href="javascript:void(0)" onclick="myJsFunc('submit');">Solicitar</a></td>
			</tr>
		</table>
	</form:form>


</body>
</html>