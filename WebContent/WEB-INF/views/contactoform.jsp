<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css" rel="stylesheet" type="text/css">
<title>Contacto</title>

</head>
<body>


<%@include file="header.jsp" %>

<form:form action="contactoform.html"  commandName="contactoForm">
	<table align="center">
		<tr>
			<td colspan="2" align="center"><h3>Formulario Contacto</h3></td>
		</tr>
		<tr>
			<td colspan="2" ><FONT color="red"><form:errors
				path="generalErrors" /></FONT></td>
		</tr>
		<tr>
			<td>Motivo:</td>
			<td><form:select path="motivo" >
				 <form:option value="Consulta">Consulta</form:option>
				 <form:option value="Dudas">Dudas</form:option>
				 <form:option value="Sugerencias">Sugerencias</form:option>
				 <form:option value="Turno">Turno</form:option>
				 <form:option value="CV">CV</form:option>
				</form:select>
			</td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Nombre:</td>
			<td><form:input path="nombre" /></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Apellido:</td>
			<td><form:input path="apellido" /></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><form:input path="email" /></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Telefono:</td>
			<td><form:input path="telefono" /></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Mensaje:</td>
			<td><form:textarea path="mensaje" /></td>
		</tr>
		
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Enviar" class="buttonStyle" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</form:form>
</body>
</html>