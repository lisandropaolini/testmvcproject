<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css"
	rel="stylesheet" type="text/css">
<title>Test MVC Project</title>
</head>
<body>

	<%@include file="header.jsp"%>

	<table align="center">
		<tr>
			<td colspan="2" align="center"><h2>Registrado con
					Ex&iacute;to.</h2></td>
		</tr>
		<tr>
			<td width="150">Usuario:</td>
			<td width="100"><core:out value="${registration.userName}" /></td>
		</tr>
		<tr>
			<td>Contrase&ntilde;a:</td>
			<td><core:out value="${registration.password}" /></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><a href="loginform.html">Back
					to Login</a></td>
		</tr>
	</table>

</body>
</html>