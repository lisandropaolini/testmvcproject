<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<%=request.getContextPath()%>/css/styles.css"
	rel="stylesheet" type="text/css">
<title>Test MVC Project</title>
</head>
<body>

	<%@include file="header.jsp"%>

	<div class="texto">

		<h3 class="title">Bienvenidos</h3>
		<div class="contenido">

			<blockquote>
				<p>Sanatorio de la ciudad de C&oacute;rdoba</p>
			</blockquote>


			<p>Sanatorio de la ciudad de C&oacute;rdoba es una entidad cuyo
				prop&oacute;sito es la de brindar a todas las familias, tanto cordobesas
				como del resto del pa&iacute;s, un servicio m&eacute;dico de alta calidad
				asistencial, a trav&eacute;s de un cuerpo m&eacute;dico de reconocida jerarqu&iacute;a
				profesional y altamente sensibilizado con las necesidades de sus
				pacientes</p>
				
			<p style="text-align: center">
				<img alt="" src="../images/ext.png" width="300px">
			</p>	
		</div>
	</div>

</body>
</html>